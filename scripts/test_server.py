#!/usr/bin/env python
#
# Test script which starts an HTTP server pretending to be an istra server
# for logins
#
# Copyright 2017 Vector Creations Ltd.

from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import logging
import random
import string

logger = logging.getLogger(__name__)

# port to listen on for dummy istra server
ISTRA_PORT = 8090

# active istra tokens
istra_tokens = set()


class IstraHandler(BaseHTTPRequestHandler):
    """Implements a dummy istra server"""
    def do_POST(self):
        content_len = int(self.headers.getheader('content-length'))
        body = json.loads(self.rfile.read(content_len))
        if self.path == '/users/login':
            return self._handle_login(body)
        elif self.path == '/users/logout':
            return self._handle_logout(body)
        else:
            self._send_response(code=404)

    def _handle_login(self, body):
        logger.info("login %s", body["user"])

        password = body["password"]
        if password.startswith("err_"):
            self._send_response(code=int(password[4:]))
            return

        # return a randomly-generated client token
        client_token = ''.join(
            random.choice(string.letters) for _ in range(20)
        )
        istra_tokens.add(client_token)

        self._send_json_response({"client_token": client_token})

    def _handle_logout(self, body):
        client_token = body["client_token"]
        logger.info("logout %s %s", body["user"], client_token)
        if client_token not in istra_tokens:
            self._send_response(code=404)
            return
        istra_tokens.remove(client_token)
        self._send_json_response({})

    def _send_json_response(self, responseobj, code=200):
        response = json.dumps(responseobj)
        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.send_header("Content-Length", len(response))
        self.end_headers()
        self.wfile.write(response)

    def _send_response(self, content="", code=200):
        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.send_header("Content-Length", len(content))
        self.end_headers()
        self.wfile.write(content)


def build_httpd():
    logging.basicConfig(level=logging.INFO)
    httpd = HTTPServer(("", ISTRA_PORT), IstraHandler)
    print(("serving at port", ISTRA_PORT))
    return httpd


def main():
    httpd = build_httpd()
    httpd.serve_forever()


if __name__ == "__main__":
    main()
