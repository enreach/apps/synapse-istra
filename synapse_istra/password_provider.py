# Copyright 2017 New Vector Ltd.
import fnmatch
import logging
import os

from twisted.internet import defer

from synapse_istra.istra_client import HttpResponseException, IstraClient
from synapse_istra.storage.tokenstore import TokenStore

logger = logging.getLogger(__name__)

ISTRA_LOGIN_TYPE = "com.centile.login.istra"
PASSWORD_LOGIN_TYPE = "m.login.password"

dir_path = os.path.abspath(os.path.dirname(__file__))


class _Config(object):
    def __init__(self):
        # URI for istra server
        self.istra_server = None


class PasswordProvider(object):
    def __init__(self, config, account_handler):
        """

        :param config (_Config): configuration
        :param account_handler (synapse.handlers.auth._AccountHandler):
        """
        self._config = config
        self._account_handler = account_handler
        self._token_store = TokenStore(account_handler)
        self._istra_client = IstraClient(config.istra_server, config)

    def get_supported_login_types(self):
        # we only support login with either a password or an istra token
        return {
            ISTRA_LOGIN_TYPE: ("password",),
            PASSWORD_LOGIN_TYPE: ("password",),
        }

    @defer.inlineCallbacks
    def check_auth(self, user, login_type, login_dict):
        """Attempt to authenticate a user against an Istra Server.

        Args:
            user (str): user name provided by the client
            login_type (str): the login type
                (ISTRA_LOGIN_TYPE|PASSWORD_LOGIN_TYPE)
            login_dict (dict): the login parameters passed by the user

        Returns:
            Deferred[str, func]: canonical user id, and optional callback
                to be called once the access token and device id are issued
        """

        logger.debug("check_auth(%s, %s, %s)", user, login_type, login_dict)
        # check that the user was registered in the synapse server first
        user_id = self._account_handler.get_qualified_user_id(user)
        logger.debug("_account_handler.get_qualified_user_id(%s) => %s", user, user_id)
        user_id = yield self._account_handler.check_user_exists(user_id)
        logger.debug("_account_handler.check_user_exists(user_id) => %s", user_id)
        if not user_id:
            defer.returnValue(None)

        try:
            logger.debug("Checking user login w/ %s ...", self._istra_client)
            client_token = yield self._istra_client.check_login(
                login_type=login_type,
                user=user_id, password=login_dict["password"],
            )
        except HttpResponseException as e:
            logger.debug(e)
            if e.code == 401:
                # fonx
                defer.returnValue(None)
            raise

        def callback(result):
            return self._token_store.store_token_map(
                result["access_token"], client_token,
            )

        defer.returnValue((user_id, callback))

    @defer.inlineCallbacks
    def on_logged_out(self, user_id, device_id, access_token):
        """Called when a user logs out on the matrix side

        Args:
            user_id (str): user corresponding to the access token
            device_id (str|None): device corresponding to the access token
            access_token (str): user's access token

        Returns:
            Deferred
        """
        # see if this access token corresponds to one of our tokens
        istra_token = yield self._token_store.delete_token_map(access_token)
        if istra_token is None:
            logger.warn("%s logged out but no known istra token", user_id)
            return
        yield self._istra_client.logout(user_id, istra_token)

    @staticmethod
    def get_db_schema_files():
        schema_dir = os.path.join(dir_path, "storage", "schema")
        directory_entries = os.listdir(schema_dir)

        files = sorted(fnmatch.filter(directory_entries, "*.sql"))
        return (
            (f, open(os.path.join(schema_dir, f), "r"))
            for f in files
        )

    @staticmethod
    def parse_config(config):
        istra_server = config.get('istra_server')
        if not istra_server:
            raise Exception(
                "synapse_istra auth missing required config value "
                "'istra_server'"
            )
        confobj = _Config()
        if istra_server[:5] == "file:":
            confobj.is_multi_istra = True
            confobj.istra_server = ""
            confobj.istra_multi_filename = istra_server[5:]
            if confobj.istra_multi_filename[-1] != '/':
                confobj.istra_multi_filename += '/'
        else:
            confobj.is_multi_istra = False
            confobj.istra_server = istra_server
            confobj.istra_multi_filename = ""
        return confobj
