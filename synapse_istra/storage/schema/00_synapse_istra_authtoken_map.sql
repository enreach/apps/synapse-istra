/* Copyright 2017 Vector Creations Ltd
 */

CREATE TABLE synapse_istra_authtoken_map (
    matrix_access_token TEXT NOT NULL,
    istra_token TEXT NOT NULL,
    UNIQUE (matrix_access_token),
    UNIQUE (istra_token)
);
