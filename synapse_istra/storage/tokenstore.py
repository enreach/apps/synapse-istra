# Copyright 2017 Vector Creations Ltd.


class TokenStore(object):
    def __init__(self, account_handler):
        self._account_handler = account_handler

    def store_token_map(self, matrix_access_token, istra_token):
        """Store a new matrix/istra token map

        Args:
            matrix_access_token (str): matrix access token
            istra_token (str): istra client token

        Returns:
            Deferred
        """
        def do_store(txn):
            sql = """INSERT INTO synapse_istra_authtoken_map
                (matrix_access_token, istra_token) VALUES (?, ?)"""
            txn.execute(sql, (matrix_access_token, istra_token))
        return self._account_handler.run_db_interaction(
            "store_synapse_istra_authtoken_map", do_store,
        )

    def delete_token_map(self, matrix_access_token):
        """Delete an existing matrix/istra token map

        Args:
            matrix_access_token (str): matrix access token

        Returns:
            Deferred[str|None]: the istra token that the matrix access token
                mapped to, if any
        """
        def do_delete(txn):
            # first fetch the mapping
            sql = """SELECT istra_token FROM synapse_istra_authtoken_map
                WHERE matrix_access_token = ?
            """
            txn.execute(sql, (matrix_access_token, ))

            row = txn.fetchone()
            if not row:
                return None
            istra_token = row[0]

            # now do the delete
            sql = """DELETE FROM synapse_istra_authtoken_map
                WHERE matrix_access_token = ?
            """
            txn.execute(sql, (matrix_access_token, ))

            return istra_token

        return self._account_handler.run_db_interaction(
            "delete_synapse_istra_authtoken_map", do_delete,
        )

    def delete_token_map_by_istra_token(self, istra_token):
        """Delete an existing matrix/istra token map

        Args:
            istra_token (str): istra client token

        Returns:
            Deferred[str|None]: the matrix access token that the istra token
                mapped to, if any
        """
        def do_delete(txn):
            # first fetch the mapping
            sql = """
                SELECT matrix_access_token FROM synapse_istra_authtoken_map
                WHERE istra_token = ?
            """
            txn.execute(sql, (istra_token, ))

            row = txn.fetchone()
            if not row:
                return None
            matrix_access_token = row[0]

            # now do the delete
            sql = """DELETE FROM synapse_istra_authtoken_map
                WHERE matrix_access_token = ?
            """
            txn.execute(sql, (matrix_access_token, ))

            return matrix_access_token

        return self._account_handler.run_db_interaction(
            "delete_synapse_istra_authtoken_map_by_istra_token", do_delete,
        )
