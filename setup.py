#!/usr/bin/env python
#
# Copyright 2017 Vector Creations Ltd.
# Copyright 2018-2020 Centile Telecom Applications S.A.S.

from setuptools import setup, find_packages

setup(
    name="synapse_istra",
    version="1.3",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    install_requires=[
        'twisted',
        'PyYAML',
        'matrix-synapse'
        # obviously we require synapse, but it's not in pypi and generally
        # isn't actually installed in the virtualenv (it's usually loaded via
        # PYTHONPATH) so we can't depend on it.
        # 'matrix-synapse',
    ],
)
